# References and lesson for learning Python

Contained here are short lessons or examples to demonstrate concepts in programming or python specifically. I hace prefixed each topic name with a number to indicate the order and therefor level of complexity. You do not have to work through in order but if for example 2_functions b_something.py confusing you should find answers working through 1_functions.

# How-To
Download this folder and read the code (and comments) in your preferred text editor.

To run code, run each file from the folder that it is placed within. Using Python directly on windows that would look something like;

1. `powershell.exe` - Open a command line
1. `cd n:\Downloads\lear-python\1.function` - Go to the first project folder
1. `python a.basic.py` - Call the python program and give it the file a.basic.py to run

## Requirements
These are taken care of by the school network.
Listed for reference by users outside the school such as home laptops or

- Python 3.* (3.6.3 at time of writing)
- Recommended Editor Visual Studio Code
    - Extension - Python by Microsoft
    - Editor Setting - trimTrailingWhitespace true keeps files tidy
    - Editor Setting - renderWhitespace to "boundary" lets you see the same text python does.
    - Editor Setting - Light Color Theme, to save documentation printer toner.
    - Editor Setting - Exclude files, __pycache__, .pyc as they're created automatically.
- Powershell, installed on windows by default. Just easier to use than cmd.exe

# Folders
`<level>_<Topic>` so for example 1.function is intriductory information for working with functions. 2.function expects you know all about 1.function and have used them yourself already

## Overview
Contains a file the encompasses all of the items within. Other folders break down concepts in this overview in more details or for future reference.

## function
Covers what functions are, how they get loaded, ways to reference them and how to pass data into and out of functions.

## import
How to use separate, smaller files to compose you complicated program. Or re-use work from other programs.