def greet(name='world'):
    print('Hello {}'.format(name))

def goodbye(name='world'):
    if name == 'world':
        print('Goodbye cruel world!')
        quit()

    print("We'll miss you {}!".format(name))

if __name__ == '__main__':
    visitor =  input('Your name: ')
    greet(visitor)
    print("I'm bored now, ending program...")
    goodbye(visitor)

# This is a common pattern in python, double underscore is used to tell the
# programmer that something not for their control is in use here.
# This example uses __name__ which python itself sets when run.
# This way we can know how this file has been called, and run (or not) different code accordinly.

# In this case we'd only want to pause execution of the program for user input when this file is used directly.
# As when python has gone and indirectly called that file (Like with an import), We shouldn't stop the program.
# As we wouldn't know why we should do that.

# This is most important when programming to clearly define separate areas of logic to think about.
# If we couldn't do this, eventually nobody would be able to keep the entire program in their head at once.
