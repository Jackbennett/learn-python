import c_useful_code as chat

user = input('What is your name? ')

chat.greet(user)

print("Never Gonna Give you up {0}".format(user))

print("Give up {0}. Exit Program".format(user))
chat.goodbye(user)

# Import lets us rename the assignment of what we're importing.
# Just like this as the name of the imported file isn't very descriptive.
# Follow the interaction and output from this program
# Take special care to note what IS NOT output from the beginning of this program.