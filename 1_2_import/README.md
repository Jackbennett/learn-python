# Imports
Programming can take a lot of thought and concentration. when trying to reason how a large program works it's useful to break actions down into separate functions whose work can be programmed one at a time.

Imports is a mechanism to break those functions down in to related groups that makes it much easier to reason about when programming.

The files in this folder demonstration exactly how python runs and exposes these files to you for reuse in other areas of a program.