def say_hello(name, times = 1):
    for count in range(times):
        print("Hello {0}".format(name))

say_hello('Johnathon', 4)

# You've seen this before lots by now I'm sure.
# You've ran this file to get "Hello Jonathon" printed 4 times.
# Now lets look at b_reuse