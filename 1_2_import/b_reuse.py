import a_functions

a_functions.say_hello('Jennifer')

# After running this file you'll see the output "Hello Johnathon" 4 times again but now "Hello Jennifer" too.
# you should read the first few paragraphs from the python manual about "the import system" https://docs.python.org/3/reference/import.html

# import will look for a file in the same folder as this one named "a_functions.py"
# Since this is python, import has the brains to add .py to the filename its looking for.

# To load the file, python must execute it.
# Anything left over from running the whole file gets pulled into this file under the name its imported as i.e. "a_functions"

# To access the definition under that name, that's separated by a dot '.'

# Using the a_functions file in this way, what kind of problem might we have later on as we write mroe code?
# hint: think about output.