# Functions
This is a basic concept that takes many names in many languages, but idea is to run a predefined set of instructions by calling a single name.
The alternative to this would mean copy/pasting code again and again whenever you want to use it.
Works OK once, but then updating it to fix a problem is a nightmare.

In Python that's called a definition and it's created by the keyword `def`.
Work your way through the files alphabetically in this folder to test this concept for yourself.
