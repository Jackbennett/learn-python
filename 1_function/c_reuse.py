# The reason to use functions is to avoid writing the same code again and again.

def hello():
    print('Hello World')

# Make a loop to say hello 5 times.
for count in range(5):
    hello()

# Repetition is all well and good but more likely you want something to change each time, even if only slightly
# To do that we can add a parameter to the definition.
# The value given to the definition is assigned the parameter name set in inside the greet brackets.
def greet(append):
    print('Hello {0}'.format(append))

name = input('Enter a name: ')
for count in range(2):
    greet(name)

# Couple of things;
# 1. greet was given the value of the variable "item" created by user input
# 2. The for loop says hello to that name twice. We don't need to set that name in code because of parameters and input.