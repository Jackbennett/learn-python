# Remember how python runs the script in order?
# What do you think will happen when this script is run?
# hint: run it.

print('start example')

showExample()

def showExample():
    print('This is an example')

# You got an error didn't you. I bet it was;
# NameError: name 'showExample' is not defined
# You'll have to change this script to make it work.
# keep in mind what the error says.
