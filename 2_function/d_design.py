# TODO: explain winners -> scores refactor of b_lambda to make the function more utilitarian

# We first covered a design choice in a_practical_reuse.py by sticking to new_list = sorted() instead of [].sort()
# Here we'll cover some more background about making those choices and why some though can save you actual hard work.

# Remember the sort key funtion in a.practical_reuse.py?
# TODO: This won't import because I can't go up to a parent folder. Might not be able to fix this.
from b_lambda import high_scores 
from c_layering import position

# Now that we've sorted the list why not create nice output to show on the screen
def scores(start=0, end=5): # Let the caller customize the amount of results returned, defaulted to 5
    rank = 1 # keep track of what order in the list we're up to
    results = []
    for (name, score) in high_scores[start:end]:
        results.append("{2:>4} Place User \"{0}\" scored: {1}".format(name, score, position(rank) ) )
        rank += 1
    # return a lsit of strings because we shouldn't assume every time this function is called we want it printed.
    return results


if __name__ == "__main__":
    # Now we've cleared the logic of the problem out of the way using fancy formatting isn't so daunting
    # The output from our program is all printed in the one place, not scattered all over. This makes changes much easier.
    # Note: print strings now use the use the .format() specified mini language: https://docs.python.org/3.4/library/string.html#formatspec
    print("{0:-^60}".format("Podium results"))
    for person in scores(end=3):
        print("{0:^60}".format(person))
    print("{0:-^60}".format("Top 10"))
    for person in scores(start=3, end=10):
        print("{0:^60}".format(person))
    print("{0:-^60}".format("Top 50"))
    for person in scores(start=10, end=50):
        print("{0:^60}".format(person))

# Output:
# -----------------------Podium results-----------------------
#               1st Place User "Name B" scored: 9
#               2nd Place User "Name B" scored: 9
#               3rd Place User "Name E" scored: 9
# ---------------------------Top 10---------------------------
#               1st Place User "Name C" scored: 8
#               2nd Place User "Name C" scored: 7
#               3rd Place User "Name A" scored: 7
#               4th Place User "Name D" scored: 7
#               5th Place User "Name C" scored: 7
#               6th Place User "Name B" scored: 7
#               7th Place User "Name C" scored: 6
# ---------------------------Top 50---------------------------
#               1st Place User "Name B" scored: 6
#               2nd Place User "Name F" scored: 5
#               3rd Place User "Name A" scored: 5
#               4th Place User "Name A" scored: 5
#               5th Place User "Name A" scored: 5
#               6th Place User "Name B" scored: 4
#               7th Place User "Name E" scored: 4
#               8th Place User "Name F" scored: 4
#               9th Place User "Name A" scored: 4
#              10th Place User "Name B" scored: 4
#              11th Place User "Name D" scored: 3
#              12th Place User "Name A" scored: 2
#              13th Place User "Name E" scored: 2
#              14th Place User "Name E" scored: 2
#              15th Place User "Name F" scored: 2
#              16th Place User "Name D" scored: 1
#              17th Place User "Name D" scored: 1
#              18th Place User "Name F" scored: 1