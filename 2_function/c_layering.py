# Great word but what is it?
# Sometime you need a simple function, don't care what its name is or that you'll reuse it anywhere, enter the lamdba

# Remember the sorted scores form b_lamdba.py?
from b_lambda import high_scores 

# Now that we've sorted the list why not create nice output to show on the screen
def winners(top=5): # Let the caller customize the amount of results returned, defaulted to 5
    rank = 1 # keep track of what order in the list we're up to
    results = []
    for (name, score) in high_scores[:top]:
        results.append("{2:>4} Place User \"{0}\" scored: {1}".format(name, score, position(rank) ) )
        rank += 1
    # return a lsit of strings because we shouldn't assume every time this function is called we want it printed.
    return results

def position(rank):
    number_list = str(rank)
    end_number = int(number_list[-2:])
    if(end_number in (1,21,31,41,51,61,71,81,91) ):
        return number_list + 'st'
    if(end_number in (2,22,32,42,52,62,72,82,92) ):
        return number_list + 'nd'
    if(end_number in (3,23,33,43,53,63,73,83,93) ):
        return number_list + 'rd'
    return number_list + 'th'

if __name__ == "__main__":
    print("Podium results")
    for person in winners(top=3):
        print(person)
    print("The rest")
    for person in winners(top=100)[3:]: #Note: We skip the first 3 results ourselves here, we didn't rely on the origonal function to offer this as a parameter
        print(person)
