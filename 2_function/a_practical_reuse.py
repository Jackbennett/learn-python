# Lets say we have a list of lists, how can we sort this list based on different items.
list_of_users_with_scores = [
    ['Name A', 2],
    ['Name B', 4],
    ['Name C', 6],
    ['Name D', 1],
    ['Name E', 4],
    ['Name F', 5],
    ['Name A', 5],
    ['Name B', 6],
    ['Name C', 7],
    ['Name D', 1],
    ['Name E', 2],
    ['Name F', 4],
    ['Name A', 7],
    ['Name B', 9],
    ['Name A', 4],
    ['Name B', 9],
    ['Name C', 8],
    ['Name D', 7],
    ['Name E', 2],
    ['Name F', 1],
    ['Name A', 5],
    ['Name B', 4],
    ['Name C', 7],
    ['Name D', 3],
    ['Name E', 9],
    ['Name F', 2],
    ['Name A', 5],
    ['Name B', 7],
]

# Note: If we use sorted() rather than the .sort() method on the list[] we get back a new list.
#       This is clearer to work with than modifying the existing lsit in place. 
#       What if I reference it somewhere else too, is that before or after its sorted?
# Note: We're after "high" scores so we already know the list needs to be high to low. Therefore we set the reverse parameter for descending results
sorted_name_scores = sorted(list_of_users_with_scores, reverse=True)

if __name__ == "__main__":
    print('Opps this is alphabetical not by score!')
    for score in sorted_name_scores:
        # Force the score row to a string otherwith the plus (+) operator is going to error about datatypes
        print("\t" + str(score)) # Note: indenting the output with a <tab> key to make it easier to read from the titles. 

# Problem: The list of user scores in sorted_scores has been sorted by the users name, that's not really the high score is it?

# sorted and .sort will take a "key" parameter to sort against rather than guess itself. So key needs to be the second item in the list
# To get the second item of whatever has been given as the key it needs to be a function.
# This function will take the row of data and return one item from it
def second_item(item):
    return item[1]

# Deceptively simple looking isn't it. Try that again
sorted_scores = sorted(list_of_users_with_scores, reverse=True, key=second_item)
# Note: We're not alling the second_item function ourselves, how can we we dont have th item. We're giving the function to sorted() for it to call it itself on each row.

# There we have it, a list sorted by the second item in the list.
if __name__ == "__main__":
    print('User names sorted by the score:')
    for score in sorted_scores:
        print("\t" + str(score))


# Further
# By the way, .sort and sorted try to guess the key and will take the fist item to sort against. which is why we got the names the first time around
# You can cheat and set the data in the first place what you'd like. But you don't get that luxary in the real world.
perfect_world = [
    [2, 'Name A'],
    [4, 'Name B'],
    [6, 'Name C'],
    [1, 'Name D'],
    [4, 'Name E'],
    [5, 'Name F'],
    [5, 'Name A'],
    [6, 'Name B'],
    [7, 'Name C'],
    [1, 'Name D'],
    [2, 'Name E'],
    [4, 'Name F'],
    [7, 'Name A'],
    [9, 'Name B'],
]

perfect_sorted_scores = sorted(perfect_world, reverse=True)

if __name__ == "__main__":
    print('Changing your dataset is the cheaty way but this does work')
    for score in perfect_sorted_scores:
        print("\t" + str(score))
