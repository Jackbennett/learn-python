# Great word but what is it?
# Sometime you need a simple function, don't care what its name is or that you'll reuse it anywhere, enter the lamdba

# Remember the sort key funtion in a_practical_reuse.py?
from a_practical_reuse import list_of_users_with_scores 

high_scores = sorted(list_of_users_with_scores, reverse = True, key = lambda item: item[1])

# Think of lambda as a shorthand which always returns its results that doesn't need to be named.
# def <anonymouse>(<parameter>):
#    return "value"

if __name__ == "__main__":
    print("Exactly the same as before but it was less code to type")
    for person in high_scores:
        print("\t" + str(person))

# Of course, there is such a thing as being too clever but less code can generally mean less mistake purely by volume.
