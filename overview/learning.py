# Concerned areas of programming/python that more than one person hasn't been aware of.
# Written from basic to advanced although really everyone should get this.
# You should run this file and follow along the comments with the program output
##

# --------------- Command Line ---------------
# First off, the command line is really important place to start.
# It makes you understand the concept of files, locations and programs the will come up later.
# Run the command line from the IT folder in start menu, cd to the location of this file. Possibly cd n:\Documents
# Just type python to run the program "python" and you'll get a REPL to the python interpreter.
## A great place to try the next section for yourself
# next see this whole file run by giving the python program the path to this file: python .\learning.py


# --------------- How to teach yourself ---------------
test = 'example of a string'
testList = ['First list element']
# print(test) # everyone knows this but they don't all use it temporaily to inspect something that isn't working.
type(test) # View variable type python is using, in this case str for string
try:
    print(1 + "1") # you get a clear error "unsupported operand type(s) for +: 'int' and 'str'"
    # That being, the plus (+) operand in python doesn't know what to do with a number and a string. You might as well be adding 1 + "potato" for all python sees.
except:
    print("I know this line errors, I don't care. Catch and just run the file")
try:
    print("1" + 1) # More tricky error with "Can't convert 'int' object to str implicitly"
    # Now that's fuzzier, but if you think implicit means guess.
    ## Python won't guess if you meant to do 1 + 1 = 2 or '1' + '1' = '11'. Some languages would have a guess i.e JavaScript
except:
    print("I know this line errors, I don't care. Catch and just run the file")

dir(test) # Look at the methods a certain type exposes to you. you do this to see what things you can do with data you already have.
# Then go a read the python docs about something you don't know or how to use one that looks useful

print(type(test)); print(dir(test))


# --------------- Lists / Arrays ---------------
listOfNumbers = [1,2,3,4,5]
# understand what the index is that's asociated with lists
#index:   0   |   1   |   2   |   3   |   4
#value: <int> | <int> | <int> | <int> | <int>
# thus, listOfNumbers[3] address' the index of the list at position 3, the value of which is 4.
print(listOfNumbers[3]) # 4

# Strings are just a list with letters inside. That's why "Example"[0] returns 'E'.


# --------------- Scope ---------------
# Scopes, the idea that variables only exist in certain contexts.
## Many student don't see that variables in python exist down and to the right.
## Thus they miss that this total needs to be outside for
total = 0
for num in listOfNumbers:
    total = total + num
print("total should be 15 here: " + str(total) )

for each in listOfNumbers:
    total = 0
    total = total + each
    print(num)
print("total should be 15 here: " + str(total) )
# which of course, does not work. But it's a bug not an error so the program runs.

# total needs to exist outside of the for block to aggriagate the value
# Did you notice that a bunch of 5's get printed? there's a 'print(num)' in the second for loop
# But num is in the previous loop not this one? leaving variables around like just asks for bugs

# function definitions let you take back control over the scope of variables
name = 'global'
def scopeAExample():
    name = 'scopeA'
    print(name)

def scopeBExample():
    global name
    name = 'scopeB'
    print(name)

print(name) # global
scopeAExample() # scopeA
print(name) # global
scopeBExample() # scopeB
print(name) # scopeB

# Note: there's a keyword 'global' to explicetly pull back in a variable from outside.
# This is not an ideal way to do things however, see fucntions.

# --------------- Functions ---------------
# Function/definition calls especially with parameters and returns.
## feedback: Completely forgeign concept to anyone in the class.
def callMe(maybe): ## accept 1 value to the fucntion about to be run, put it in the variable 'maybe'
    return 'I will call you: ' + str(maybe) ## Give back a string that also uses the provided value

def callMeBroke(maybe): ## accept 1 value to the fucntion about to be run, put it in the variable 'maybe'
    'I will call you: ' + str(maybe) ## Give back a string that also uses the provided value

print( callMe('ishmael') ) ## print whatever callMe gives us back, in this case a string.

print(callMeBroke('Sue')) # as callMeBroke doens't return a value,
## sidenote: it will implicitly return the none value type of 'None'

# Using parameters bring clarity to variable values that global variables does not offer.


# --------------- Objects / dictionary ---------------
# I've not seen anyone do this without me explaining it.
## feedback: data stucture new to the class. Seems to understand when shown but I don't think anyone used it.
### Especially important as they used csv.reader rather than csv.DICTreader which would have made easier code to read.

## Make an empty list
allThings = []

## Make an object. Well python datatype this is a dict (dictionary). Other languages call it a hashtable. There's many names.
## Just think "KEY" : VALUE, rather than a list INDEX : VALUE
thing = {
    'width': 20,
    'height': 10,
    'round': 'Ish',
    'potato': True
}

for times in 1, 2, 3:
    allThings.append(thing)

if thing['potato']:
    print('this is indeed a potato')

print(thing['round']) # access the value of things property 'round'
print(allThings[0]) # Show the first row of the list.
print(allThings) # Show all things in a nicer format than print alone does.

# Multi-dimentional Array
# You _could_ make a value that works similar to an object out of an array
hardToReadThing = [
    ['width', 20],
    ['height', 10],
    ['round', 'Ish'],
    ['potato', True]
]
print(hardToReadThing[0][1])

#look at the output
# hardToReadThing
## [['width', 20], ['height', 10], ['round', 'Ish'], ['potato', True]]
# thing
# {'width': 20, 'height': 10, 'round': 'Ish', 'potato': True}

# Ok, what about accessing values?
# hardToReadThing[0][1]
## 20
# thing['width']
# 20

# Now what valus is hardToReadThing[2][1] going to be? Not so easy.
# what about some hypotherical drawBox function?
## drawBox( size[1][1], size[0][1] )
# Is that width by hight or hight by width? compare to;
## drawBox( size['width'], size['height'] )

# --------------- Example ---------------
# Compounding Problems.
# While this does work, demonstrates quite a savere lack of being able to read and UNDERSTAND what is going on.
import csv
with open('learning.py') as datastream:
    for line in datastream:
        # print each line or something
        continue

def searchForLine():
    comments = 0
    code = 0
    with open('learning.py') as datastream:
        for line in datastream:
            if line.startswith('#'):
                # this was a comment
                comments += 1
            else:
                # this was code
                code += 1
    # For bonus points, can you spot the bug?
    return {'comments': comments, 'code': code}

thisFile = searchForLine()
print(str(thisFile['comments']) + ' lines are comments and ' + str(thisFile['code']) + ' is code.')

# things to note:
# 1. Repeatedly opening the same file
# 2. Imports a library but it's never actually used
# 3. Missing concept of saving data in a variable for later use

## --------------- Advanced Concepts ---------------
# Separation of concerns with module import
# Datatypes
# Error Handling

# Why not learn how this works
print('{comments} lines are comments and {code} is code.'.format(**searchForLine()))
